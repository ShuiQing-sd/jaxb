# jaxb

#### 介绍
bean 和 xml 相互转化 

* 实现对枚举类型根据需求进行转换，特别适用于在实体类中有大量枚举定义，且需要枚举值转移时使用
* 在xml转化为实体时，对xml根据schema进行数据校验

#### 软件架构
无


#### 安装教程

无

#### 使用说明

无


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
