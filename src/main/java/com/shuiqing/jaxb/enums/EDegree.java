package com.shuiqing.jaxb.enums;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.shuiqing.jaxb.adpter.EnumCodeAdapter;

/**
 * 政治面貌枚举类
 *
 * @author lhr
 */
@XmlJavaTypeAdapter(EDegree.CodeAdapter.class)
public enum EDegree {

    ONE(1, "一级"),

    TWO(2, "二级"),

    THREE(3, "三级");

    private Integer code;

    private String text;

    EDegree(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    /**
     * 定义内部类，实现枚举类型的转化
     */
    static class CodeAdapter extends EnumCodeAdapter<EDegree> {
        public CodeAdapter() {
            super(EDegree.class);
        }
    }
}
