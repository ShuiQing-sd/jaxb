package com.shuiqing.jaxb.enums;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.shuiqing.jaxb.adpter.EnumCodeAdapter;

/**
 * 人员性别
 *
 * @author lhr
 */
@XmlJavaTypeAdapter(EGender.CodeAdapter.class)
public enum EGender {

    MALE(0, "男"),

    FEMALE(1, "女"),

    SECRET(2, "保密");

    /**
     * 序列值
     */
    private Integer code;

    /**
     * 名称
     */
    private String text;

    EGender(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    /**
     * 定义内部类，实现枚举类型的转化
     */
    static class CodeAdapter extends EnumCodeAdapter<EGender> {
        public CodeAdapter() {
            super(EGender.class);
        }
    }

}
