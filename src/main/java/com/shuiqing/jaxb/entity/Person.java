package com.shuiqing.jaxb.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.shuiqing.jaxb.adpter.DateAdapter;
import com.shuiqing.jaxb.enums.EDegree;
import com.shuiqing.jaxb.enums.EGender;

import java.io.Serializable;
import java.util.Date;

/**
 * 人员
 *
 * @author lhr
 */
@XmlType(propOrder = {"name", "birthday","gender", "degree","remark"})
@XmlRootElement(name = "person")
public class Person implements Serializable {

    private static final long serialVersionUID = -2478210259556423451L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 人员名称
     */
    private String name;

    /**
     * 出生日期
     * 格式： yyyy-MM-dd HH:mm:ss
     */
    private Date birthday;

    /**
     * 人员性别
     */
    private EGender gender;

    /**
     * 政治面貌
     */
    private EDegree degree;

    /**
     * 备注
     */
    private String remark;

    @XmlTransient
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlElement(name="name", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name="birthday", required = true)
    @XmlJavaTypeAdapter(DateAdapter.class)
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @XmlElement(name="gender", required = true)
    public EGender getGender() {
        return gender;
    }

    public void setGender(EGender gender) {
        this.gender = gender;
    }

    @XmlElement(name="degree")
    public EDegree getDegree() {
        return degree;
    }

    public void setDegree(EDegree degree) {
        this.degree = degree;
    }

    @XmlElement(name="remark")
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
