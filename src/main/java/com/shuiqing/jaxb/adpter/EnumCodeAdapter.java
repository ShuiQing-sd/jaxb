package com.shuiqing.jaxb.adpter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 实体bean和xml相互转化时，对枚举类进行处理
 *
 * @param <E> 自定义的枚举类型
 * @author lhr
 */
public class EnumCodeAdapter<E extends Enum> extends XmlAdapter<Integer, E> {

    private Class<E> clazz;

    public EnumCodeAdapter(Class<E> clazz) {
        this.clazz = clazz;
    }

    /**
     * 将xml中的枚举类型的序列号映射为定义的枚举类型
     *
     * @param code xml中枚举类型对应的序列号
     * @return 返回相应的枚举，如果找不到相应序列化对应的枚举，则返回空
     */
    @Override
    public E unmarshal(Integer code) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        if (code == null) {
            return null;
        }

        Method method = clazz.getDeclaredMethod("getCode");
        Enum[] enums = clazz.getEnumConstants();
        for (Enum item : enums) {
            if (code.equals(method.invoke(item))) {
                return clazz.cast(item);
            }
        }
        return null;
    }

    /**
     * 将bean中的枚举值转换为xml中的序列号
     *
     * @param value 枚举值
     * @return 返回相应枚举值的序列号
     */
    @Override
    public Integer marshal(E value) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = clazz.getDeclaredMethod("getCode");
        return (Integer) method.invoke(value);
    }

}