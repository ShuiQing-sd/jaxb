package com.shuiqing.jaxb.adpter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间格式转换
 *
 * @author lhr
 */
public class DateAdapter extends XmlAdapter<String, Date> {

    private static final Logger logger = LoggerFactory.getLogger(DateAdapter.class);

    @Override
    public Date unmarshal(String v) {
        if (StringUtils.isBlank(v)) {
            return null;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            return sdf.parse(v);
        } catch (ParseException e) {
            logger.debug("时间转换错误", e);
            return null;
        }
    }

    @Override
    public String marshal(Date v) {
        if (null == v) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(v);
    }
}
