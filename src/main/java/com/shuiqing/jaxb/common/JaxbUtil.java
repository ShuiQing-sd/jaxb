package com.shuiqing.jaxb.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * jaxb 实现bean和xml相互转化的工具类
 *
 * @author lhr
 */
public class JaxbUtil {

    private static final Logger logger = LoggerFactory.getLogger(JaxbUtil.class);


    /**
     * 将xml字符串转化为实体bean
     *
     * @param xmlStr 字符串
     * @param clazz  对象类型
     * @return 对象实例，如果转化失败，则返回null
     */
    public static <T> T xml2Object(String xmlStr, Class<T> clazz) {
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            return clazz.cast(unmarshaller.unmarshal(new StringReader(xmlStr)));
        } catch (JAXBException e) {
            logger.error("xml转化为实体bean发生异常", e);
            return null;
        }
    }


    /**
     * 将xml字符串转化为实体bean
     *
     * @param xmlStr         字符串
     * @param schemaFileName schema文件名
     * @param clazz          对象类型
     * @return 对象实例，如果转化失败，则返回null
     */
    public static <T> T xml2Object(String xmlStr, String schemaFileName, Class<T> clazz) {
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(JaxbUtil.class.getClassLoader().getResource(schemaFileName));

            unmarshaller.setSchema(schema);
            return clazz.cast(unmarshaller.unmarshal(new StringReader(xmlStr)));
        } catch (SAXException e) {
            logger.error("schema校验xml发生异常", e);
            return null;
        } catch (JAXBException e) {
            logger.error("xml转化为实体bean发生异常", e);
            return null;
        }
    }


    /**
     * 将实体bean转化为xml格式的字符串
     *
     * @param object 待转换的实体bean
     * @return 返回xml格式的字符串，转化失败，则抛出异常
     */
    public static String object2Xml(Object object) {
        try {
            StringWriter writer = new StringWriter();
            JAXBContext context = JAXBContext.newInstance(object.getClass());
            Marshaller marshal = context.createMarshaller();

            // 格式化输出
            marshal.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // 编码格式,默认为utf-8
            marshal.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

            // 是否省略xml头信息
            marshal.setProperty(Marshaller.JAXB_FRAGMENT, false);
            marshal.marshal(object, writer);
            String xmlStr = new String(writer.getBuffer());
            return xmlStr;
        } catch (JAXBException e) {
            logger.error("数据换失败，请检查数据内容", e);
            throw new RuntimeException("数据换失败，请检查数据内容");
        }
    }


    /**
     * 将实体bean转化为xml格式的字符串
     *
     * @param object         待转换的实体bean
     * @param schemaFileName schema文件名
     * @return 返回xml格式的字符串，转化失败，则抛出异常
     */
    public static String object2Xml(Object object, String schemaFileName) {
        String xmlStr = JaxbUtil.object2Xml(object);
        if (!validateXML(xmlStr, schemaFileName)) {
            logger.error("数据格式不正确，请核对数据格式");
            throw new RuntimeException("数据格式不正确，请核对数据格式");
        }
        return xmlStr;
    }

    /**
     * 根据Schema xsd文件验证 xml 文件
     *
     * @param xmlStr         xml字符串
     * @param schemaFileName schema的文件名
     * @return 校验通过返回true, 校验失败返回false
     */
    public static boolean validateXML(String xmlStr, String schemaFileName) {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(JaxbUtil.class.getClassLoader().getResource(schemaFileName));
            Reader xmlReader = new StringReader(xmlStr);
            Source xmlSource = new StreamSource(xmlReader);

            Validator validator = schema.newValidator();
            validator.validate(xmlSource);
            return true;
        } catch (SAXException e) {
            logger.error("实体转化为xml异常", e);
            return false;
        } catch (IOException e) {
            logger.error("schema校验转化后的数据异常", e);
            return false;
        }
    }

}
