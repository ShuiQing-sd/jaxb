package com.shuiqing.jaxb.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.IOException;

/**
 * 根据实体生成schema的工具类
 *
 * @author lhr
 */
public class SchemaUtil {

    private static final Logger logger = LoggerFactory.getLogger(JaxbUtil.class);

    /**
     * 根据实体类生成schema
     *
     * @param clazz 实体类类型
     */
    public static void generateSchema(Class clazz) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
        jaxbContext.generateSchema(new Resolver());
    }

}
